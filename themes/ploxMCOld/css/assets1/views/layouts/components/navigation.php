<?php
                $items = array();

                $simple = (Yii::app()->theme && in_array(Yii::app()->theme->name, array('simple', 'mobile', 'platform')));
                $items[] = array('label'=>Yii::t('mc', '<p style="text-align: center;"><i class="fa fa-home" aria-hidden="true"></i></p>'), 'url'=>array('/site/page', 'view'=>'home'));

                if (@Yii::app()->params['installer'] !== 'show')
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<p style="text-align: center;"><i class="fa fa-list"></i></p>'),
                        'url'=>array('/server/index', 'my'=>1),
                        'visible'=>(!Yii::app()->user->isGuest || Yii::app()->user->showList),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<p style="text-align: center;"><i class="fa fa-users" aria-hidden="true"></i></p>'),
                        'url'=>array('/user/index'),
                        'visible'=>(Yii::app()->user->isStaff()
                            || !(Yii::app()->user->isGuest || (Yii::app()->params['hide_userlist'] === true) || $simple)),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<p style="text-align: center;"><i class="fa fa-user" aria-hidden="true"></i></p>'),
                        'url'=>array('/user/view', 'id'=>Yii::app()->user->id),
                        'visible'=>(!Yii::app()->user->isStaff() && !Yii::app()->user->isGuest
                            && ((Yii::app()->params['hide_userlist'] === true) || $simple)),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<p style="text-align: center;"><i class="fa fa-cog" aria-hidden="true"></i></p>'),
                        'url'=>array('/daemon/index'),
                        'visible'=>Yii::app()->user->isSuperuser(),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<p style="text-align: center;"><i class="fa fa-info-circle" aria-hidden="true"></i></p>'),
                        'url'=>array('/site/report'),
                        'visible'=>empty(Yii::app()->params['admin_email']),
                    );
                }
                if (Yii::app()->user->isGuest)
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<p style="text-align: center;"><i class="fa fa-sign-in" aria-hidden="true"></i></p>'),
                        'url'=>array('/site/login'),
                    );
                }
                else
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<p style="text-align: center;"><i class="fa fa-sign-out" aria-hidden="true"></i></p>'),
                        'url'=>array('/site/logout'),
                    );
                }
                $items[] = array(
                    'label'=>Yii::t('mc', 'About'),
                    'url'=>array('/site/page', 'view'=>'about'),
                    'visible'=>$simple,
                    'itemOptions'=>array('style'=>'float: right'),
                );
                
                
                $this->widget('zii.widgets.CMenu', array(
                    'items'=>$items,
                    'encodeLabel' => false,
                    'htmlOptions'=>array('class' => 'leftMenu')
                ));
                ?>
