<?php
/**
 *
 *   Copyright © 2010-2018 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/
?>

<!doctype html>
<html lang="en">
<!--
 -
 -   Copyright © 2010-2018 by xhost.ch GmbH
 -
 -   All rights reserved.
 -
 -->
<head>
    <meta content="initial-scale=1.0, width=device-width, user-scalable=yes" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <link rev="made" href="mailto:support@plox.host">
    <meta name="description" content="PloxHost | Control Panel">
    <meta name="keywords" content="PloxHost, Minecraft, server, management, control panel, hosting">
    <meta name="author" content="xhost.ch GmbH">
    <meta charset="UTF-8" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo Theme::css('style.css') ?>" media="screen, projection" />

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="<?php echo Theme::css('') ?>/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->


		<link href="<?php echo Theme::css('') ?>/assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/vendors/custom/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="<?php echo Theme::css('') ?>/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins -->
		<link href="<?php echo Theme::css('') ?>/assets/demo/default/skins/header/base/brand.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/demo/default/skins/brand/brand.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Theme::css('') ?>/assets/demo/default/skins/aside/light.css" rel="stylesheet" type="text/css" />
		
        <link rel="shortcut icon" href="https://plox.host/assets/img/favicon.ico"/>
        <link rel="shortcut icon" href="https://plox.host/assets/img/favicon.ico" type="image/x-icon" />

		<link href="<?php echo Theme::css('') ?>/assets1/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo Theme::css('') ?>/assets1/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
		<link href="<?php echo Theme::css('') ?>/assets1/css/style.css" rel="stylesheet">
		<link href="<?php echo Theme::css('') ?>/assets1/css/ploxhost.css" rel="stylesheet">
		<link href="<?php echo Theme::css('') ?>/assets1/css/responsive.css" rel="stylesheet">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/v4-shims.css">


    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

	<!-- begin::Body -->
	<body class="k-header--fixed k-header-mobile--fixed k-aside--enabled k-aside--fixed k-aside-secondary--enabled">



<nav class="tiny-nav">
	<div class="containerx">
		<div class="floated">
			<ul>
				<li><a href="https://billing.plox.host">Billing Area</a></li>
				<li><a href="https://help.plox.host">Help Center</a></li>
				<li><a href="https://plox.host/mcp">Multicraft Login</a></li>
				<li><a href="https://ns346097.ip-178-33-235.eu:2083/">Cpanel Login</a></li>
				
			</ul>
		</div>
	</div>
</nav>
<nav class="navxbar navxbar-default navxbar-fixed-top">
	<div class="containerx">
		<div class="navxbar-header">
			<button type="button" class="navxbar-toggle collapsexd" data-toggle="collapsex" data-target="#collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navxbar-brand" href="">
				<img src="<?php echo Theme::css('') ?>/assets1/img/plox-logo-long.png" alt="Plox.host A Minecraft Hosting Company." style="height: 100%;">
			</a>
		</div>
		<div class="collapsex navxbar-collapse" id="collapse-1">
			<ul class="navx navxbar-nav navxbar-right">
				<li><a href="https://plox.host/">Home</a></li>
				<li><a href="https://plox.host/minecraft-hosting.php">Minecraft</a></li>
				<li><a href="https://plox.host/vps-hosting.php">VPS Hosting</a></li>
				<li><a href="https://plox.host/kvm-hosting.php">KVM Hosting</a></li>
	          <li class="dropdownx">
			  <a href="#" class="dropdownx-toggle" data-toggle="dropdownx" role="button" aria-haspopup="true" aria-expanded="false">Dedicated <span class="caretx"></span></a>
					<ul class="dropdownx-menu">
						<li><a href="https://plox.host/dedicated-servers.php">Dedicated</a></li>
						<li><a href="https://plox.host/hardware.php">Hardware</a></li>
					</ul>
				</li>
                 <li class="dropdownx">
			  <a href="#" class="dropdownx-toggle" data-toggle="dropdownx" role="button" aria-haspopup="true" aria-expanded="false">Web Hosting <span class="caretx"></span></a>
					<ul class="dropdownx-menu">
						<li><a href="https://plox.host/web-hosting.php">Web Hosting</a></li>
						<li><a href="https://plox.host/reseller-web-hosting.php">Reseller Plans</a></li>
					</ul>
				</li>
				<li><a href="https://plox.host/about-us.php">About Us</a></li>
			</ul>
		</div>
	</div>
</nav>

<div id="header-home">
  <div class="containerx">
    <div class="rowx">
      <div class="colx-md-10">
        <h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
        <h3></h3>
      </div>
    </div>
  </div>
</div>

<div id="legal">
