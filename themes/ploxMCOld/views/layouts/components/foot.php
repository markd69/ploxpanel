
<script src="<?php echo Theme::js('bootstrap.min.js') ?>"></script>
<script src="<?php echo Theme::js('multicraft.js') ?>"></script>


</div>


<footer1 id="footer1">
	<div class="containerx">
		<div class="rowx">
			<div class="colx-md-1">
				<div class="icon"></div>
			</div>
			<div class="colx-md-2">
				<h1>Company</h1>
				<ul class="list-unstyled">
					<li><a href="https://plox.host/about-us.php">About Us</a></li>
					<li><a href="https://plox.host/about-us.php">Server Information</a></li>
					<li><a href="https://plox.host/partners">Partners</a></li>
					<li><a href="https://plox.host/about-us.php">Our Staff</a></li>
				</ul>
			</div>
			<div class="colx-md-2">
				<h1>Services</h1>
				<ul class="list-unstyled">
					<li><a href="https://plox.host/minecraft-hosting.php">Minecraft Hosting</a></li>
					<li><a href="https://plox.host/web-hosting.php">Web Hosting</a></li>
					<li><a href="https://plox.host/vps-hosting.php">VPS Hosting</a></li>
					<li><a href="https://plox.host/kvm-hosting.php">KVM Hosting</a></li>
					<li><a href="https://billing.plox.host/cart.php?a=add&domain=register">Domain Search</a></li>
					<li><a href="https://plox.host/hytale.php">Hytale Hosting <b>COMMING SOON</b></a></li>
				</ul>
			</div>
			<div class="colx-md-2">
				<h1>Support</h1>
				<ul class="list-unstyled">
					<li><a href="https://help.plox.host">Troubleshoot</a></li>
					<li><a href="https://billing.plox.host/contact.php">Partnership Contact</a></li>
					<li><a href="https://status.plox.host"></a></li>
					<li><a href="https://billing.plox.host/submitticket.php">Submit a Ticket</a></li>
				</ul>
			</div>
			<div class="colx-md-2">
				<h1>Legal</h1>
				<ul class="list-unstyled">
					<li><a href="https://plox.host/terms-of-service.php">Terms of Service</a></li>
					<li><a href="https://plox.host/privacy-policy.php">Privacy Policy</a></li>
					<li><a href="https://billing.plox.host/contact.php">Report Abuse</a></li>
				</ul>
			</div>
			<div class="colx-md-3">
				<h1>Payment Information</h1>
				<img src="<?php echo Theme::css('') ?>/assets1/img/payments.png" alt="paypal, visa, mastercard, american express, discover">
				<h1>Our Social Media</h1>
				
				<a href=""><i class="fa fa-facebook-square fa-fw fa-2x" aria-hidden="true"></i></a>
				<a href="https://twitter.com/PloxHost"><i class="fa fa-twitter-square fa-fw fa-2x" aria-hidden="true"></i></a>
				
			</div>
		</div>
	</div>
</footer1>
<div id="sub-footer">
	<div class="containerx">
		<div class="rowx text-center">
			<p> Copyright &copy; 2019 PloxHost. All Rights Reserved.</p>
                        <p> PloxHost is a division of ZoomingWork LLC </p>
		</div>
	</div>
		<a href="//www.dmca.com/Protection/Status.aspx?ID=5965cc1e-2ab5-4cec-9b91-ae5658c00003" title="DMCA.com Protection Status" class="dmca-badge"> <img src="//images.dmca.com/Badges/dmca_protected_sml_120m.png?ID=5965cc1e-2ab5-4cec-9b91-ae5658c00003" alt="DMCA.com Protection Status"></a> <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="//images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>

</div>




		<!--begin:: Global Mandatory Vendors -->
		<script src="<?php echo Theme::css('') ?>/assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?php echo Theme::css('') ?>/assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?php echo Theme::css('') ?>/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo Theme::css('') ?>/assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?php echo Theme::css('') ?>/assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?php echo Theme::css('') ?>/assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?php echo Theme::css('') ?>/assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?php echo Theme::css('') ?>/assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
		<script src="<?php echo Theme::css('') ?>/assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->


		<!--begin::Global Theme Bundle -->
		<script src="<?php echo Theme::css('') ?>/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<script src="<?php echo Theme::css('') ?>/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="<?php echo Theme::css('') ?>/assets/app/scripts/custom/dashboard.js" type="text/javascript"></script>

		<!--end::Page Scripts -->

		<!--begin::Global App Bundle -->
		<script src="<?php echo Theme::css('') ?>/assets/app/scripts/bundle/app.bundle.js" type="text/javascript"></script>

		<!--end::Global App Bundle -->
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="<?php echo Theme::css('') ?>/assets1/js/bootstrap.js"></script>
	<script src="<?php echo Theme::css('') ?>/assets1/js/morph.js"></script>
	<script src="<?php echo Theme::css('') ?>/assets1/js/ploxhost.js"></script>
	
	
</body>
</html>