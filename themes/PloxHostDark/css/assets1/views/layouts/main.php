<?php
/**
 *
 *   Copyright © 2010-2016 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/
?>
<?php $this->renderPartial('//layouts/components/head'); ?>
<div class="container">

    <div class="row" id="content"><?php echo $content; ?></div>
</div>
<?php $this->renderPartial('//layouts/components/foot'); ?>
