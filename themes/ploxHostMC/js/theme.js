// # ERROR
// popover doesn't appear to be initiating at the right time.
// =====
// $(document).ready(function() {
// $('.hint').popover({
//     trigger: 'hover',
//     delay: {show: 100, hide: 500},
//     html: true,
//     placement: 'left',
//     container: 'body'
// });
// });

//
$(document).ready(function () {
    $("#yw2 > li > a").each(function (ind, el) {
        // console.log(el);
        let $el = $(el);
        el.dataset.bbicon = $("img", $el)[0].src;
        el.dataset.html = $el.html();
    });
    let lastWidth = 0;

    function checkForChanges() {
        // console.log("Checking for changes in k_aside");
        let $element = $("#k_aside");
        let curWidth = $element.css('width').replace(/px$/, "");
        if (curWidth !== lastWidth) {
            if (curWidth > 200) {
                $("#yw2 > li > a").each(function (ind, el) {
                    $(el).html(el.dataset.html);
                });
            } else {
                $("#yw2 > li > a").each(function (ind, el) {
                    $(el).html("&nbsp;<img src=\"" + el.dataset.bbicon + "\" alt=\"\">");
                });
            }
        }
        lastWidth = curWidth;
    }

    setInterval(checkForChanges, 500)
});