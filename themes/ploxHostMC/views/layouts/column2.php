<?php
/**
 *
 *   Copyright © 2010-2016 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/
?>


<?php

function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}

if(!Yii::app()->user->isGuest){

    $id = Yii::app()->user->id;
    $results = Yii::app()->db->createCommand()->
    select('email')->
    from('user')->
    where('id=:id', array(':id'=>$id))->
    limit(1)->
    queryAll();
    $email = $results['0']['email'];

} else {
    $email = '';
}

$size = 120;
$grav_url = get_gravatar($email, $size);
if (Yii::app()->user->isGuest && (Yii::app()->controller->id == "site"))
    return $this->renderPartial('//layouts/mini', array('content'=>$content));

$this->beginContent('//layouts/main');

?>
<style>
	 /* Tooltip container */
.tooltipx {
  position: relative;
  display: inline-block;
  border-bottom: 0px dotted black; /* If you want dots under the hoverable text */
}

/* Tooltip text */
.tooltipx .tooltipxtext {
  visibility: hidden;
  width: 120px;
  background-color: #555;
  color: #fff;
  text-align: center;
  padding: 5px 0;
  border-radius: 6px;

  /* Position the tooltip text */
  position: absolute;
  z-index: 1;
  bottom: 150%;
  left: 50%;
  margin-left: -60px;

  /* Fade in tooltip */
  opacity: 0;
  transition: opacity 0.3s;
}

/* Tooltip arrow */
.tooltipx .tooltipxtext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

/* Show the tooltip text when you mouse over the tooltip container */
.tooltipx:hover .tooltipxtext {
  visibility: visible;
  opacity: 1;
}
</style>


		<!-- begin:: Page -->


		<!-- begin:: Header Mobile -->
		<div id="k_header_mobile" class="k-header-mobile  k-header-mobile--fixed ">
			<div class="k-header-mobile__logo">
				<a href="index.php">
				</a>
			</div>
			<div class="k-header-mobile__toolbar">
				<button class="k-header-mobile__toolbar-toggler k-header-mobile__toolbar-toggler--left" id="k_aside_mobile_toggler"><span></span></button>
				<button class="k-header-mobile__toolbar-toggler" id="k_header_mobile_toggler"><span></span></button>

			</div>
		</div>

		<!-- end:: Header Mobile -->










		<div class="k-grid k-grid--hor k-grid--root">
			<div class="k-grid__item k-grid__item--fluid k-grid k-grid--ver k-page">


				<!-- begin:: Aside -->
				<button class="k-aside-close " id="k_aside_close_btn"><i class="la la-close"></i></button>
				<div class="k-aside  k-aside--fixed 	k-grid__item k-grid k-grid--desktop k-grid--hor-desktop" id="k_aside">

					<!-- begin:: Aside -->
					<div class="k-aside__brand	k-grid__item " id="k_aside_brand">
						<div class="k-aside__brand-logo">
							<a href="index.php">
							</a>
						</div>
						<div class="k-aside__brand-tools">
							<button class="k-aside__brand-aside-toggler k-aside__brand-aside-toggler--left" id="k_aside_toggler"><span></span></button>
						</div>
					</div>

					<!-- end:: Aside -->



					<!-- begin:: Aside Menu -->
					<div class="xn-side-bar k-aside-menu-wrapper	k-grid__item k-grid__item--fluid" id="k_aside_menu_wrapper">
						<div id="k_aside_menu" class="k-aside-menu " data-kmenu-vertical="1" data-kmenu-scroll="1" data-kmenu-dropdown-timeout="500">
              <div id="sidebarx">
<?php
        $this->beginWidget('zii.widgets.CPortlet', array(
            'hideOnEmpty'=>false
        ));
        $this->widget('application.components.Menu', array(
            'items'=>$this->menu,
            'htmlOptions'=>array('class'=>'operations'),
        ));
        $this->endWidget();
    ?>
  						</div>
						</div>
					</div>


					<!-- end:: Aside -->
				</div>

				<!-- end:: Aside -->
				<div class="k-grid__item k-grid__item--fluid k-grid k-grid--hor " id="k_wrapper">
					<!-- begin:: Header -->
					<div id="k_header" class="xn-header-panel-top-x">
						<button class="k-header-menu-wrapper-close" id="k_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

						<div class="k-header-menu-wrapper" id="k_header_menu_wrapper">
							<div id="k_header_menu" class="k-header-menu k-header-menu-mobile ">

								<div class="  hidden-sm hidden-xs" style="margin-top:20px;">
								<ul class="k-menu__nav">
                <?php
                $items = array();

                $simple = (Yii::app()->theme && in_array(Yii::app()->theme->name, array('simple', 'mobile', 'platform')));

                $items[] = array('label'=>Yii::t('mc', '<div class="tooltipx xnx"><i class="icon01 flaticon2-shelter" style="color:white;" title="Home"></i><span class="tooltipxtext">Home</span></div>'),
                'url'=>array('/site/page', 'view'=>'home'),
                'linkOptions' => array('class'=>'k-menu__link'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel')


                );

                if (@Yii::app()->params['installer'] !== 'show')
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<div class="tooltipx xnx"><i class="icon01 flaticon2-website" style="color:white;" title="Servers"></i><span class="tooltipxtext">Servers</span></div> '),
                        'url'=>array('/server/index', 'my'=>1),
                        'linkOptions' => array('class'=>'k-menu__link'),
                        'visible'=>(!Yii::app()->user->isGuest || Yii::app()->user->showList),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<div class="tooltipx xnx"><i class="icon01 flaticon2-user" style="color:white;" title="Users"></i><span class="tooltipxtext">Users</span></div>'),
                        'url'=>array('/user/index'),
                        'visible'=>(Yii::app()->user->isStaff()
                            || !(Yii::app()->user->isGuest || (Yii::app()->params['hide_userlist'] === true) || $simple)),
                            'linkOptions' => array('class'=>'k-menu__link'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<div class="tooltipx xnx"><i class="icon01 flaticon2-user" style="color:white;" title="Profile"></i><span class="tooltipxtext">Profile</span></div>'),
                        'url'=>array('/user/view', 'id'=>Yii::app()->user->id),
                        'visible'=>(!Yii::app()->user->isStaff() && !Yii::app()->user->isGuest
                            && ((Yii::app()->params['hide_userlist'] === true) || $simple)),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<div class="tooltipx xnx"><i class="icon01 flaticon2-settings" style="color:white;" title="Settings"></i><span class="tooltipxtext">Settings</span></div>'),
                        'url'=>array('/daemon/index'),
                        'visible'=>Yii::app()->user->isSuperuser(),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<div class="tooltipx xnx"><i class="icon01 flaticon2-help" style="color:white;" title="Report"></i><span class="tooltipxtext">Report</span></div>'),
                        'url'=>array('/site/report'),
                        'visible'=>!empty(Yii::app()->params['admin_email']),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                }
                if (Yii::app()->user->isGuest)
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<div class="tooltipx xnx"><i class="icon01 flaticon2-user" style="color:white;" title="Login"></i><span class="tooltipxtext">Login</span></div>'),
                        'url'=>array('/site/login'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                }
                else
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<div class="tooltipx xnx"><i class="icon01 flaticon2-power-button-symbol " style="color:white;" title="Logout"></i><span class="tooltipxtext">Logout</span></div>'),
                        'url'=>array('/site/logout'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                }
                $items[] = array(
                    'label'=>Yii::t('mc', 'About'),
                    'url'=>array('/site/page', 'view'=>'about'),
                    'visible'=>$simple,
                    'itemOptions'=>array('style'=>'float: right'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                );


                $this->widget('zii.widgets.CMenu', array(

                    'items'=>$items,
                    'encodeLabel' => false,
                    'htmlOptions'=>array('class' => 'k-menu__nav'),

                ));
                ?>
								</ul>
							</div>
								<div class="hidden-md hidden-lg" style="margin-top:20px;">
									<br><br>
								<ul class="k-menu__nav" style="z-index:2000;">

                <?php
                $items = array();

                $simple = (Yii::app()->theme && in_array(Yii::app()->theme->name, array('simple', 'mobile', 'platform')));

                $items[] = array('label'=>Yii::t('mc', '<i class="icon01 flaticon2-shelter"></i>&nbsp;&nbsp; Home'),
                'url'=>array('/site/page', 'view'=>'home'),
                'linkOptions' => array('class'=>'k-menu__link'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel')


                );

                if (@Yii::app()->params['installer'] !== 'show')
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<i class="icon01 flaticon2-website"></i> &nbsp;&nbsp;Servers'),
                        'url'=>array('/server/index', 'my'=>1),
                        'linkOptions' => array('class'=>'k-menu__link'),
                        'visible'=>(!Yii::app()->user->isGuest || Yii::app()->user->showList),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<i class="icon01 flaticon2-user"></i>&nbsp;&nbsp; Users'),
                        'url'=>array('/user/index'),
                        'visible'=>(Yii::app()->user->isStaff()
                            || !(Yii::app()->user->isGuest || (Yii::app()->params['hide_userlist'] === true) || $simple)),
                            'linkOptions' => array('class'=>'k-menu__link'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<i class="icon01 flaticon2-user"></i>&nbsp;&nbsp; Profile'),
                        'url'=>array('/user/view', 'id'=>Yii::app()->user->id),
                        'visible'=>(!Yii::app()->user->isStaff() && !Yii::app()->user->isGuest
                            && ((Yii::app()->params['hide_userlist'] === true) || $simple)),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<i class="icon01 flaticon2-settings"></i> &nbsp;&nbsp;Settings'),
                        'url'=>array('/daemon/index'),
                        'visible'=>Yii::app()->user->isSuperuser(),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                    $items[] = array(
                        'label'=>Yii::t('mc', '<i class="icon01 flaticon2-help"></i> &nbsp;&nbsp;Report'),
                        'url'=>array('/site/report'),
                        'visible'=>!empty(Yii::app()->params['admin_email']),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                }
                if (Yii::app()->user->isGuest)
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<i class="icon01 flaticon2-user"></i>&nbsp;&nbsp; Login'),
                        'url'=>array('/site/login'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                }
                else
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<i class="icon01 flaticon2-power-button-symbol "></i>&nbsp;&nbsp; Logout'),
                        'url'=>array('/site/logout'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                    );
                }
                $items[] = array(
                    'label'=>Yii::t('mc', 'About'),
                    'url'=>array('/site/page', 'view'=>'about'),
                    'visible'=>$simple,
                    'itemOptions'=>array('style'=>'float: right'),
                'itemOptions'=>array('class'=>'k-menu__item  k-menu__item--submenu k-menu__item--rel'),
                'linkOptions' => array('class'=>'k-menu__link'),
                );


                $this->widget('zii.widgets.CMenu', array(

                    'items'=>$items,
                    'encodeLabel' => false,
                    'htmlOptions'=>array('class' => 'k-menu__nav'),

                ));
                ?>
								</ul>
							</div>
							</div>
						</div>
						<!-- end: Header Menu -->
						<!-- begin:: Header Topbar -->
						<div class="k-header__topbar">
							<!--begin: User bar -->
							<div class="k-header__topbar-item k-header__topbar-item--user">
								<div class="k-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px -2px">
									<div class="k-header__topbar-user">
										<span class="k-header__topbar-username k-hidden-mobile">
											Your Account
											</span>
										<img alt="Pic" src="<?= $grav_url ?>" />
									</div>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-md">
											<div class="k-user-card__pic xn-drop-user">
												<img alt="Pic" src="<?= $grav_url ?>" />
												<div class="k-user-card__name">Your Account</div>
											</div>
               <?php
                $items = array();

                $simple = (Yii::app()->theme && in_array(Yii::app()->theme->name, array('simple', 'mobile', 'platform')));


                if (@Yii::app()->params['installer'] !== 'show')
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<span class="k-nav__link-icon"><i class="flaticon2-website"></i></span>
												<span class="k-nav__link-text">Servers</span>'),
                        'url'=>array('/server/index', 'my'=>1),
                        'linkOptions' => array('class'=>'k-nav__link'),
                        'visible'=>(!Yii::app()->user->isGuest || Yii::app()->user->showList),
                'itemOptions'=>array('class'=>'k-nav__item'),
                    );

                    $items[] = array(
                        'label'=>Yii::t('mc', '<span class="k-nav__link-icon"><i class="flaticon2-user"></i></span>
												<span class="k-nav__link-text">Profile</span>'),
                        'url'=>array('/user/view', 'id'=>Yii::app()->user->id),
                        'visible'=>(!Yii::app()->user->isStaff() && !Yii::app()->user->isGuest
                            && ((Yii::app()->params['hide_userlist'] === true) || $simple)),
                'itemOptions'=>array('class'=>'k-nav__item'),
                'linkOptions' => array('class'=>'k-nav__link'),
                    );


                }
                if (Yii::app()->user->isGuest)
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<span class="k-nav__link-icon"><i class="flaticon2-user"></i></span>
												<span class="k-nav__link-text">Login</span>'),
                        'url'=>array('/site/login'),
                'itemOptions'=>array('class'=>'k-nav__item'),
                'linkOptions' => array('class'=>'k-nav__link'),
                    );
                }
                else
                {
                    $items[] = array(
                        'label'=>Yii::t('mc', '<span class="k-nav__link-icon"><i class="flaticon2-close-cross-circular-interface-button"></i></span>
												<span class="k-nav__link-text">Logout</span>'),
                        'url'=>array('/site/logout'),
                'itemOptions'=>array('class'=>'k-nav__item'),
                'linkOptions' => array('class'=>'k-nav__link'),
                    );
                }


                $this->widget('zii.widgets.CMenu', array(

                    'items'=>$items,
                    'encodeLabel' => false,
                    'htmlOptions'=>array('class' => 'k-nav k-margin-b-10'),

                ));
                ?>
								</div>
							</div>
							<!--end: User bar -->
							<!--end: Quick panel toggler -->
						</div>
						<!-- end:: Header Topbar -->
					</div>

					<!-- end:: Header -->

					<!-- begin:: Content -->
					<div class="k-content	k-grid__item k-grid__item--fluid k-grid k-grid--hor" id="k_content">

						<!-- begin:: Content Body -->
						<div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">



								<div class="col-md-12">

									<div class="k-portlet">
<div class="k-portlet__head">
											<div class="k-portlet__head-label">
												<h3 class="k-portlet__head-title">
													<?php
        $this->beginWidget('zii.widgets.CPortlet', array(
            'title'=>CHtml::encode(end($this->breadcrumbs)),
            'hideOnEmpty'=>false
        ));

        $this->endWidget();
    ?>
													</h3>
											</div>
											<div class="k-portlet__head-toolbar">
												<div class="k-portlet__head-toolbar-wrapper">

												</div>
											</div>
										</div>
										<div class="k-portlet__body">
											<div class="k-portlet__content">

    <?php
        if (count($this->breadcrumbs) > 1) {
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'homeLink'=>'',
                'links'=>$this->breadcrumbs,
                'tagName' => 'ol',
                'htmlOptions' => array('class'=>'breadcrumb'),
                'separator' => '',
                'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
                'inactiveLinkTemplate' => '<li class="active">{label}</li>',
            ));
        }
    ?>
    <?php echo $content; ?>


</div>
</div></div>
</div>


						</div>

						<!-- end:: Content Body -->
					</div>

					<!-- end:: Content -->



				</div>

			</div>
		</div>




		<!-- begin::Scrolltop -->
		<!-- <div id="k_scrolltop" class="k-scrolltop">
			<i class="la la-arrow-up"></i>
		</div> -->

		<!-- end::Scrolltop -->
		<!-- begin::Global Config -->


		<!-- end::Global Config -->
        <!-- <script>
            const toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');
            const currentTheme = localStorage.getItem('theme');

            if (currentTheme) {
                document.documentElement.setAttribute('data-theme', currentTheme);

                if (currentTheme === 'dark') {
                    toggleSwitch.checked = true;
                }
            }

            function switchTheme(e) {
                if (e.target.checked) {
                    document.documentElement.setAttribute('data-theme', 'dark');
                    localStorage.setItem('theme', 'dark');
                }
                else {        document.documentElement.setAttribute('data-theme', 'light');
                    localStorage.setItem('theme', 'light');
                }
            }

            toggleSwitch.addEventListener('change', switchTheme, false);
        </script> -->
















<?php $this->endContent(); ?>
