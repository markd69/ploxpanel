<?php
/**
 *
 *   Copyright © 2010-2018 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/
?>
<?php $this->renderPartial('//layouts/components/head'); ?>
    <div id="mini">
        

        <div class="rowx" id="content"><?php echo $content; ?></div>
    </div>
<?php $this->renderPartial('//layouts/components/foot'); ?>
