<?php
/**
 *
 *   Copyright © 2010-2016 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/
?>

<?php

function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}

if(!Yii::app()->user->isGuest){

    $id = Yii::app()->user->id;
    $results = Yii::app()->db->createCommand()->
    select('email')->
    from('user')->
    where('id=:id', array(':id'=>$id))->
    limit(1)->
    queryAll();
    $email = $results['0']['email'];

} else {
    $email = '';
}

$size = 120;
$grav_url = get_gravatar($email, $size);

if (Yii::app()->user->isGuest && (Yii::app()->controller->id == "site"))
    return $this->renderPartial('//layouts/mini', array('content'=>$content));

$this->beginContent('//layouts/main');

?>
<div class="col-md-1" id="mainMenu">
    <p style="text-align: center; margin-top: 25px; margin-bottom: 20px;"><img width="60" height="60" style="border: 2px solid #ffffff;" class="img-circle" src="<?= $grav_url ?>"></p>
    <?php $this->renderPartial('//layouts/components/navigation'); ?>
</div>
<div class="col-md-2" id="sidebar">
    <?php
        $this->beginWidget('zii.widgets.CPortlet', array(
            'title'=>CHtml::encode(end($this->breadcrumbs)),
            'hideOnEmpty'=>false
        ));
        $this->widget('application.components.Menu', array(
            'items'=>$this->menu,
            'htmlOptions'=>array('class'=>'operations'),
        ));
        $this->endWidget();
    ?>

        <?php
        if(!$this->notice == '')
        {
            echo '<div class="notifications">';
            echo $this->notice;
            echo '</div>';
        }
        ?>
</div>
<div class="col-md-9 col-toppad">
    <div id="col-toppad">
    <?php
        if (count($this->breadcrumbs) > 1) {
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'homeLink'=>'',
                'links'=>$this->breadcrumbs,
                'tagName' => 'ol',
                'htmlOptions' => array('class'=>'breadcrumb'),
                'separator' => '',
                'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
                'inactiveLinkTemplate' => '<li class="active">{label}</li>',
            ));
        }
    ?>
    <?php echo $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>
