<?php
/**
 *
 *   Copyright © 2010-2016 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/
?>
<!doctype html>
<html lang="en">
<!--
 -
 -   Copyright © 2010-2016 by xhost.ch GmbH
 -
 -   All rights reserved.
 -
 -->
<head>
    <meta content="initial-scale=1.0, width=device-width, user-scalable=yes" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <link rev="made" href="https://plox.host/">
    <meta name="description" content="PloxHost | Server Control Panel">
    <meta name="keywords" content="PloxHost, Minecraft, server, management, control panel, hosting">
    <meta name="author" content="xhost.ch GmbH">
    <meta name="theme-color" content="#19aba6">
    <meta content="https://i.imgur.com/SSb2OS2.png" property="og:image">
    <meta charset="UTF-8" />
    <link rel="shortcut icon" href="<?php echo  Yii::app()->request->baseUrl; ?>/favicon.ico" />

    <link rel="stylesheet" type="text/css" href="<?php echo Theme::css('style.css') ?>" media="screen, projection" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body id="bodymini">
<div id="header2"></div>
<div id="page">
