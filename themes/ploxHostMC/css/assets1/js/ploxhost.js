$('.huge').each(function() {
  var $this = $(this),
      countTo = $this.attr('data-count');
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },
  {
    duration: 8000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
      //alert('finished');
    }
  });
});
//jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navxbar").offset().top > 70) {
        $(".navxbar-fixed-top").addClass("color-override");
    } else {
        $(".navxbar-fixed-top").removeClass("color-override");
    }
});
$(window).scroll(function() {
    if ($(".navxbar").offset().top < 70) {
        $(".navxbar-fixed-top").addClass("push-override");
    } else {
        $(".navxbar-fixed-top").removeClass("push-override");
    }
});
$("#js-rotating").Morphext({
    // The [in] animation type. Refer to Animate.css for a list of available animations.
    animation: "fadeIn",
    // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
    separator: ",",
    // The delay between the changing of each phrase in milliseconds.
    speed: 2000,
    complete: function () {
        // Called after the entrance animation is executed.
    }
});
