<?php
session_start();

if(!isset($_SESSION['delete_server']) && !isset($_POST['line'])) {
    die();
}

if($_POST['line'] === "0") {
    echo file_get_contents("/var/www/html/multicraft/protected/logs/".$_SESSION['delete_server'].".txt");
} else {
    $lines = file("/var/www/html/multicraft/protected/logs/".$_SESSION['delete_server'].".txt");
    foreach ($lines as $linenum => $line) {
        if($linenum >= (int) $_POST['line']) {
            echo $line."<br />\n";
        }
    }
}