<?php
function dateUTC($format, $timestamp = null)
{
    if ($timestamp === null) $timestamp = time();

    $tz = date_default_timezone_get();
    date_default_timezone_set('UTC');

    $result = date($format, $timestamp);

    date_default_timezone_set($tz);
    return $result;
}
echo dateUTC("Y-m-d H:i:s",time());