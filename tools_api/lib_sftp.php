<?php

class SFTPConnection
{
    private $connection;
    private $sftp;

    public function __construct($host, $port = 22) {
        $this->connection = @ssh2_connect($host, $port);
        if (!$this->connection)
            throw new Exception("Could not connect to $host on port $port.");
    }

    public function login($username, $password) {
        if (!@ssh2_auth_password($this->connection, $username, $password))
            throw new Exception("Could not authenticate with username $username " .
                "and password $password.");

        $this->sftp = @ssh2_sftp($this->connection);
        if (!$this->sftp)
            throw new Exception("Could not initialize SFTP subsystem.");
    }

    public function uploadFile($local_file, $remote_file) {
        echo "Opening stream...\n";
        $sftp = $this->sftp;
        $stream = @fopen("ssh2.sftp://$sftp$remote_file", 'w');

        if (!$stream)
            throw new Exception("Could not open file: $remote_file");
        echo "Opened\n";
        $data_to_send = @file_get_contents($local_file);
        if ($data_to_send === false)
            throw new Exception("Could not open local file: $local_file.");
        echo "Getting local file contents\n";
        if (@fwrite($stream, $data_to_send) === false)
            throw new Exception("Could not send data from file: $local_file.");
        echo "Wrote to remote storage\n";
        @fclose($stream);
        echo "Closed stream\n";
        return true;
    }

    /* HIGHLY EXPERIMENTAL */
    public function deleteFile($remote_file) {
        $sftp = $this->sftp;
        return unlink("ssh2.sftp://$sftp$remote_file");
    }
    function formatURL($remote_file) {
        $sftp = $this->sftp;
        return "ssh2.sftp://$sftp$remote_file";
    }

    public function receiveFile($remote_file) {
        $sftp = $this->sftp;
        $stream = @fopen("ssh2.sftp://$sftp$remote_file", 'r');
        if (!$stream)
            throw new Exception("Could not open file: $remote_file");
        $contents = fread($stream, filesize("ssh2.sftp://$sftp$remote_file"));
        @fclose($stream);
        return $contents;
    }
}