<?php
try {
    include_once "lib_sftp.php";
    $mysql_ = [
        "servername" => "localhost",
        "username" => "root",
        "password" => "cumberland",
        "dbname" => "panel",
        "tbname" => "custom_backups"
    ];
    $ftp = [
        "address" => "23.238.204.89",
        "port" => 22,
        "username" => "root",
        "password" => "Cumberland1.!",
        "folder" => "/root/test-backup/"
    ];
    if (isset($_POST['backup'])) {
        $conn = new mysqli($mysql_["servername"], $mysql_["username"], $mysql_["password"], $mysql_["dbname"]);
        if ($conn->connect_error) {
            echo "<h1>MySQL connection failed</h1>" . "<h4><a href='${_SERVER['HTTP_REFERER']}'>Go back</a></h4>";
            // go back
            $url = $_SERVER['HTTP_REFERER'];
            $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . 'backup=pending';
            $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . 'error=mysql_conn';
            header("Location: " . $url);
        } else {
            $date = date("Y-m-d H:i:s", time());
            $serverid=$conn->real_escape_string($_POST['serverid']);
            $owneremail=$conn->real_escape_string($_POST['owneremail']);
            $sql = <<<SQL
INSERT INTO `panel`.`custom_backups` (`serverid`, `status`, `filename`, `filesize`, `owner_email`, `date`) VALUES (${serverid},0,null,null,"${$owneremail}","${date}");
SQL;
            $result = $conn->query($sql);
            // go back
            $url = $_SERVER['HTTP_REFERER'];
            $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . 'backup=pending';
            header("Location: " . $url);
        }
    } else if (isset($_POST['restore'])) {

        // go back
        $url = $_SERVER['HTTP_REFERER'];
        $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . 'backup=restoring';
        header("Location: " . $url);
    } else if (isset($_POST['download'])) {
        echo "Not functional";
        return;
//        $sftp = new SFTPConnection($ftp['address'], $ftp['port']);
//        $sftp->login($ftp['username'], $ftp['password']);
//        $remote_file = $ftp['folder'] . $_POST['filename'];
////        $file = $sftp->receiveFile($remote_file);
////        if ($file === false) {
////            echo "Failed to fetch file from remote server.";
////        } else {
////            header('Content-Type: application/octet-stream');
////            header("Content-Transfer-Encoding: Binary");
////            header("Content-disposition: attachment; filename=\"backup.zip\"");
////            echo $file;
////        }
//        header('Content-Type: application/octet-stream');
//        header("Content-Transfer-Encoding: Binary");
//        header("Content-disposition: attachment; filename=\"backup.zip\"");
//
//        $curl = curl_init();
//        curl_setopt($curl, CURLOPT_URL, $sftp->formatURL($remote_file)); #input
//        curl_setopt($curl, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
//        curl_setopt($curl, CURLOPT_USERPWD, $ftp['username'].":".$ftp['password']);
//        curl_exec($curl);
//        curl_close($curl);
////        echo $remote_file;
//        return;
    } else if (isset($_POST['delete'])) {
        $conn = new mysqli($mysql_["servername"], $mysql_["username"], $mysql_["password"], $mysql_["dbname"]);
        if ($conn->connect_error) {
            echo "<h1>MySQL connection failed</h1>\n<h4><a href='${_SERVER['HTTP_REFERER']}'>Go back</a></h4>";
            // go back
            $url = $_SERVER['HTTP_REFERER'];
            $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . 'backup=failed_delete__mysql';
            header("Location: " . $url);
        } else {
            $serverid=$conn->real_escape_string($_POST['serverid']);
            $date__=$conn->real_escape_string($_POST['date']);
            $sql = <<<SQL
UPDATE `${mysql_['dbname']}`.`${mysql_['tbname']}` SET `status` = -2 WHERE `serverid`=${serverid} AND `date` = "${date__}";
SQL;
            $result = $conn->query($sql);
            // go back
            $url = $_SERVER['HTTP_REFERER'];
            $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . 'backup=deleted';
            header("Location: " . $url);
//            print_r($sql);
//            echo "\n\n";
//            print_r($result);
        }
    } else {
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }
    $conn->close();
} catch (Exception $e) {
    print_r($e);
}