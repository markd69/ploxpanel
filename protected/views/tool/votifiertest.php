<?php
$this->pageTitle = Yii::app()->name.' - '.Yii::t('mc', 'Votifier test');
$this->breadcrumbs=array(
    Yii::t('mc', 'Servers')=>array('server/index'),
    Yii::t('mc', 'Tools'),
);
$this->menu=array(
    array(
        'label'=>Yii::t('mc', 'MOTD Creator'),
        'url'=>array('tool/motdcreator', 'sv'=>$sv),
        'icon'=>'command_new',
    ),
    array(
        'label'=>Yii::t('mc', 'Votifier test'),
        'url'=>array('tool/votifiertest', 'sv'=>$sv),
        'icon'=>'command_new',
    ),
    array(
        'label'=>Yii::t('mc', 'Back'),
        'url'=>array('server/view', 'id'=>$sv),
        'icon'=>'back',
    ),
);
?>
<form method="post" accept-charset="utf-8" action="/multicraft/tools_api/votifier.php">
    <div class="form-group">
        <label class="control-label" for="username">Minecraft username:</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><img id="avatar" src="https://minotar.net/helm/Steve/20.png" alt="Steve" width="20" height="20"></span>
            </div>
            <input type="text" name="username" id="username" class="form-control" placeholder="Steve" pattern="[a-zA-Z0-9_]{3,16}" required="" autofocus="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label" for="address">IP address / hostname:</label>
        <input type="text" name="address" id="address" class="form-control" placeholder="123.45.67.89 / mc.myserver.net" required="" disabled="" value="<?php echo $ip; ?>">
    </div>
    <div class="form-group">
        <label class="control-label" for="port">Port:</label>
        <input type="number" name="port" id="port" class="form-control" placeholder="8192" value="<?php echo $port; ?>" min="1024" max="65535" required="">
    </div>
    <div class="form-group">
        <label class="control-label" for="key">Public key:</label>
        <textarea name="key" id="key" class="form-control" placeholder="You can find the key in /plugins/Votifier/rsa/public.key" rows="3" required=""></textarea>
    </div>
    <button type="submit" name="votifier" class="btn btn-primary btn-lg">Send test vote</button>
</form>
<?php
echo CHtml::script('
var username_regex = /^[a-zA-Z0-9_]{3,16}$/;
document.querySelector("#username").onblur=function() {
    if (username_regex.exec(document.querySelector("#username").value)) document.querySelector("#avatar").src=\'https://minotar.net/helm/\' + document.querySelector("#username").value + \'/20.png\';
    else document.querySelector("#avatar").src=\'https://minotar.net/helm/Steve/20.png\';
}
');