<?php
$this->pageTitle = Yii::app()->name.' - '.Yii::t('mc', 'MOTD Creator');
$this->breadcrumbs=array(
    Yii::t('mc', 'Servers')=>array('server/index'),
    Yii::t('mc', 'Tools'),
);
$this->menu=array(
    array(
        'label'=>Yii::t('mc', 'MOTD Creator'),
        'url'=>array('tool/motdcreator', 'sv'=>$sv),
        'icon'=>'command_new',
    ),
    array(
        'label'=>Yii::t('mc', 'Votifier test'),
        'url'=>array('tool/votifiertest', 'sv'=>$sv),
        'icon'=>'command_new',
    ),
    array(
        'label'=>Yii::t('mc', 'Back'),
        'url'=>array('server/view', 'id'=>$sv),
        'icon'=>'back',
    ),
);
?>
    <div id="content">
        <form method="post" accept-charset="utf-8" action="#motd" id="motd">
            <div class="form-group">
                <label for="motd-raw">Type your MOTD here. Use the toolbar or type the <a href="#" title="Show the color codes" id="toggle-color-codes" role="button">color codes</a>:</label>
                <div class="btn-toolbar" id="motd-toolbar" role="toolbar">
                    <div class="btn-group btn-group-sm btn-colors" role="group">
                        <button type="button" class="btn btn-light" data-color="0" title="" data-original-title="Black"><span style="background-color: #000000;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="1" title="" data-original-title="Dark Blue"><span style="background-color: #0000AA;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="2" title="" data-original-title="Dark Green"><span style="background-color: #00AA00;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="3" title="" data-original-title="Dark Aqua"><span style="background-color: #00AAAA;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="4" title="" data-original-title="Dark Red"><span style="background-color: #AA0000;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="5" title="" data-original-title="Dark Purple"><span style="background-color: #AA00AA;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="6" title="" data-original-title="Gold"><span style="background-color: #FFAA00;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="7" title="" data-original-title="Gray"><span style="background-color: #AAAAAA;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="8" title="" data-original-title="Dark Gray"><span style="background-color: #555555;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="9" title="" data-original-title="Blue"><span style="background-color: #5555FF;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="a" title="" data-original-title="Green"><span style="background-color: #55FF55;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="b" title="" data-original-title="Aqua"><span style="background-color: #55FFFF;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="c" title="" data-original-title="Red"><span style="background-color: #FF5555;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="d" title="" data-original-title="Light Purple"><span style="background-color: #FF55FF;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="e" title="" data-original-title="Yellow"><span style="background-color: #FFFF55;">&nbsp;</span></button>
                        <button type="button" class="btn btn-light" data-color="f" title="" data-original-title="White"><span style="background-color: #FFFFFF;">&nbsp;</span></button>
                    </div>
                    <div class="btn-group btn-group-sm" role="group">
                        <button type="button" class="btn btn-light" data-color="l" data-original-title="" title=""><span style="font-weight: bold;">Bold</span></button>
                        <button type="button" class="btn btn-light" data-color="n" data-original-title="" title=""><span style="text-decoration: underline;">Underline</span></button>
                        <button type="button" class="btn btn-light" data-color="o" data-original-title="" title=""><span style="font-style: italic;">Italic</span></button>
                        <button type="button" class="btn btn-light" data-color="m" data-original-title="" title=""><span style="text-decoration: line-through;">Strikethrough</span></button>
                        <button type="button" class="btn btn-light" data-color="k" data-original-title="" title="">Obfuscated</button>
                        <button type="button" class="btn btn-light" data-color="r" data-original-title="" title="">Reset</button>
                    </div>
                </div>
                <textarea name="motd" id="motd-raw" class="form-control" rows="2" style="resize: none;" required="" autofocus="">A Minecraft Server
&amp;4Here is another line</textarea>
                <label style="margin-top: 5px;"><input type="checkbox" name="center" id="motd_center"> Center the lines</label>
            </div>
            <div class="form-group">
                <label>Live preview:</label>
                <div class="minecraft" id="motd-preview">A Minecraft Server
<span style="color: #AA0000">Here is another line</span></div>
                <p class="help-block">Minecraft might render the MOTD slightly different. Test before you use it.</p>
            </div>
            <div class="form-group">
                <label for="motd-config">For the <code>server.properties</code> file:</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">motd=</span>
                    </div>
                    <input type="text" id="motd-config" class="form-control copy" readonly="" value="A Minecraft Server\n\u00A74Here is another line">
                </div>
            </div>
            <div class="form-group">
                <label for="motd-bungeecord">For <a href="http://www.spigotmc.org/wiki/bungeecord/" target="_blank">BungeeCord's</a> <code>config.yml</code> file:</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">motd:</span>
                    </div>
                    <input type="text" id="motd-bungeecord" class="form-control copy" readonly="" value="&quot;A Minecraft Server\n&amp;4Here is another line&quot;">
                </div>
            </div>
            <div class="form-group">
                <label for="motd-serverlistplus">For <a href="http://www.spigotmc.org/resources/serverlistplus.241/" target="_blank">ServerListPlus'</a> <code>ServerListPlus.yml</code> file:</label>
                <textarea id="motd-serverlistplus" class="form-control copy" rows="3" style="resize: none;" readonly="">- |-
  A Minecraft Server
  &amp;4Here is another line</textarea>
            </div>
            <hr>
        </form>
    </div>
<?php
echo CHtml::script('
function update_motd() {
	var form = $(\'#motd\').serialize();

	$.post(\'/multicraft/tools_api/motd.php\', form, function(motd){
	$(\'#motd-preview\').html(JSON.stringify(motd))
		$(\'#motd-preview\').html(motd[\'html\']);
		$(\'#motd-config\').val(motd[\'config\']);
		$(\'#motd-bungeecord\').val(motd[\'bungeecord\']);
		$(\'#motd-serverlistplus\').val(motd[\'serverlistplus\']);
//		$(\'#motd-link\').val(motd[\'link\']);
	});
}
window.addEventListener(\'load\', function () {
var username_regex = /^[a-zA-Z0-9_]{3,16}$/;

$.fn.selectRange = function(start, end){
    if(!end) end = start;
    return this.each(function(){
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd(\'character\', end);
            range.moveStart(\'character\', start);
            range.select();
        }
    });
};

$(function(){
	\'use strict\';

	var motd_raw = $(\'#motd-raw\');
	$(\'#motd_center\').change(function(e){
		update_motd();
	});

	motd_raw.keyup(function(e){
		update_motd();
	});

	motd_raw.keydown(function(e){
		if(e.keyCode == 13 && $(this).val().split("\n").length >= $(this).attr(\'rows\'))
			return false;
	});

	$(\'#motd-toolbar button\').click(function(e){
		var caretPos = motd_raw[0].selectionStart;
		var textAreaTxt = motd_raw.val();
		var txtToAdd = \'&\' + $(this).data(\'color\');

		motd_raw.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos)).focus();
		update_motd();
		motd_raw.selectRange(caretPos + 2);
	});

	/*$(\'#motd-toolbar button\').tooltip({
		container: \'body\'
	});*/

	$(\'#toggle-color-codes\').click(function(e){
		$(\'#motd-toolbar\').toggleClass(\'toggled\');
		e.preventDefault();
	});
});
update_motd();
setTimeout(function(){
$(\'.copy\').click(function(){
    this.select();
});
},1e3);
});
');
echo CHtml::css('

#content textarea {
	resize: vertical;
}

#content h1 {
	font-size: 3.1rem;
	margin: 20px 0;
}

#content {
	border: 0px solid #DDDDDD;
	border-top: none;
	padding: 15px;
	padding-top: 20px;
}

#content footer {
	margin-top: 5px;
}

#content .minecraft {
	background-color: black;
	color: #AAAAAA;
	display: inline-block;
	font-family: Minecraft Regular,monospace;
	letter-spacing: 1px;
	padding: 5px;
	white-space: pre;
	width: 100%;
}

#content .copy {
	cursor: copy !important;
}

#content .input-group {
	width: 100%;
}

#content .btn-colors .btn span {
	display: inline-block;
	outline: 1px solid rgb(204,204,204);
	width: 18px;
}

#content .btn-toolbar .btn-group {
	margin-bottom: 10px;
}

#content #motd-toolbar.toggled .btn::after {
	content: " (&" attr(data-color) ")";
}

#content #whitelist-errors, #whitelist-display {
	display: none;
}

#content .top-banner p {
	font-size: 60%;
}
');
