<?php
/**
 *
 *   Copyright © 2010-2018 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/


$this->pageTitle=Yii::app()->name . ' - '.Yii::t('mc', 'Home');
$this->breadcrumbs=array(
    Yii::t('mc', 'Home'),
);


$this->menu=array(
    array(
        'label'=>Yii::t('mc', 'Welcome'),
        'url'=>array('', 'view'=>'home'),
        'icon'=>'welcome',
    ),
    array(
        'label'=>Yii::t('mc', 'Get Support'),
        'url'=>'#',
        'icon'=>'help',
        'linkOptions'=>array(
            'submit'=>'https://help.plox.host/',
            'confirm'=>Yii::t('mc', "You are leaving the panel\n Press confirm to be redirected to the support site.")),
    ),
    array(
        'label'=>Yii::t('mc', 'About'),
        'url'=>array('', 'view'=>'about'),
        'icon'=>'about',
    ),
);

?>
<br/>
<?php echo Yii::t('mc', 'Welcome to <b>{Plox Panel}</b>, our Minecraft server control panel.', array('{Plox Panel}'=>CHtml::link('Plox Panel', 'https://plox.host/'))) ?> If you have any questions or need support, you can contact the team by clicking the button on the sidebar.<br/>
<a class="twitter-timeline" data-width="500" data-height="500" data-theme="dark" href="https://twitter.com/PloxHost?ref_src=twsrc%5Etfw">Tweets by PloxHost</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><?php if (Yii::app()->user->isGuest && @file_exists(Theme::themeFilePath('images/icons/login_large.png'))): ?>

<?php 

        $uri = $_SERVER['REQUEST_URI'];

        if ($uri == '/multicraft/index.php?r=site/page&view=home') 
        {
            header("Location: index.php?r=site/login");
            die;
        }

    ?>
<div class="introbar">
    <?php
        if (Yii::app()->user->isGuest) {
            echo CHtml::link(Theme::img('icons/login_large.png').' <span>'.Yii::t('mc', 'Login').'</span>', array('site/login'));
        }

        echo CHtml::link(Theme::img('icons/help_large.png').' <span>'.Yii::t('mc', 'Get Support').'</span>', 'https://help.plox.host/', array(
            'submit'=>'https://help.plox.host/',
            'confirm'=>Yii::t('mc', "You're leaving the Plox Panel\nPress confirm to be redirected to the support site.")
            )
        );

        echo CHtml::link(Theme::img('icons/about_large.png').' <span>'.Yii::t('mc', 'About').'</span>', array('', 'view'=>'about'))
    ?>
</div>
<?php endif ?>

<?php if (Yii::app()->params['demo_mode'] == 'enabled'): ?>
<br/>
<br/>
<div class="infoBox">
<h3>Demo mode</h3>
<h4><?php echo CHtml::link('Log in here', array('site/login')) ?></h4>
<br/>
The Demo Mode does not cover all of the features Multicraft provides as there is no real server running behind it. Please use the free version of Multicraft for evaluation.<br/>
</div>
<br/>
<br/>
<div class="infoBox">
<h3>Disclaimer</h3>
The content of this Demo Mode installation can be edited by anyone. We are not responsible in any way for any of the content on this installation.<br/>
The content will be reset on a regular basis.<br/>
</div>

<?php endif ?> 
