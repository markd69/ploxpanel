<?php
/*
 * Modification coded by Michelle Winters
 */

if($model->owner === Yii::app()->user->id || Yii::app()->user->isStaff()){
    $_SESSION['reset'] = true;
}

header('Location: index.php?r=server/log&id='.$model->id);
?>