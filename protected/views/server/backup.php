<?php
/**
 *
 *   Copyright © 2010-2018 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('mc', 'Backup server');

$this->breadcrumbs = array(
    Yii::t('mc', 'Servers') => array('index'),
    $model->name => array('view', 'id' => $model->id),
    Yii::t('mc', 'Backup'),
);

$this->menu = array(
    array(
        'label' => Yii::t('mc', 'Back'),
        'url' => array('server/view', 'id' => $model->id),
        'icon' => 'back',
    ),
);
function statusCal($status_number) {
    switch ($status_number) {
        case 1:
            return "Done";
        case 0:
            return "Pending";
        case -1:
            return "Error";
        case -2:
            return "Pending delete";
        default:
            return "Unknown";
    }
}

$aaa = CustomBackup::model()->findBySql("SELECT * FROM panel.custom_backups WHERE `serverid`=" . $model->id . " AND `automated`=1 ORDER BY `date`;");
$eee = CustomBackup::model()->findAllBySql("SELECT * FROM panel.custom_backups WHERE `serverid`=" . $model->id . " ORDER BY `date`;");
?>

<div class="container-fluid">
    <h1>Backup Management</h1>
    <h3>You are only allowed to store two backups at a time. You may delete your backups and create new ones at any time.</h3>
    <h3>You currently have <code><?php echo count($eee); ?>/2</code> backups used.</h3>
    <?php if (count($eee) < 2): ?>
        <form action="/multicraft/tools_api/backup.php"  method="post">
            <input type="text" hidden value="<?php echo $model->id ?>" name="serverid">
            <input type="text" hidden value="<?php echo $model->getDaemon($model->id) ?>" name="daemonid">
            <input type="text" hidden value="<?php echo $owner->email ?>" name="owneremail">
            <button class="btn btn-primary" type="submit" name="backup">Backup</button>
        </form>
    <?php endif; ?>
    <hr>
    <table class="detail-view" id="yw0">
        <tbody>
        <tr class="titlerow odd">
            <th>Backup Capture Date</th>
            <th>Backup Size</th>
            <th>Status</th>
            <th>Available Actions</th>
        </tr>
        <?php if (isset($eee[0])): ?>
            <tr class="even">
                <th><label><?php echo $eee[0]->date ?> UTC</label></th>
                <td><label><?php echo $eee[0]->filesize ?></label></td>
                <td><label><?php echo statusCal($eee[0]->status) ?></label></td>
                <td>
                    <form action="/multicraft/tools_api/backup.php" method="post">
                        <input type="text" hidden value="<?php echo $eee[0]->date ?>" name="date">
                        <input type="text" hidden value="<?php echo $model->id ?>" name="serverid">
                        <input type="text" hidden value="<?php echo $eee[0]->filename ?>" name="filename">
                        <input type="text" hidden value="<?php echo $eee[1]->date ?>" name="date">
                        <button class="btn btn-primary" name="restore" type="submit">Restore Backup</button>
                        <button class="btn btn-primary" name="download" type="submit">Download Backup</button>
                        <button class="btn btn-primary" name="delete" type="submit">Delete Backup</button>
                    </form>
                </td>
            </tr>
        <?php endif; ?>
        <?php if (isset($eee[1])): ?>
            <tr class="odd">
                <th><label><?php echo $eee[1]->date ?> UTC</label></th>
                <td><label><?php echo $eee[1]->filesize ?></label></td>
                <td><label><?php echo statusCal($eee[1]->status) ?></label></td>
                <td>
                    <form action="/multicraft/tools_api/backup.php" method="post">
                        <input type="text" hidden value="<?php echo $eee[1]->date ?>" name="date">
                        <input type="text" hidden value="<?php echo $model->id ?>" name="serverid">
                        <input type="text" hidden value="<?php echo $eee[1]->filename ?>" name="filename">
                        <input type="text" hidden value="<?php echo $eee[1]->date ?>" name="date">
                        <button class="btn btn-primary" name="restore" type="submit">Restore Backup</button>
                        <button class="btn btn-primary" name="download" type="submit">Download Backup</button>
                        <button class="btn btn-primary" name="delete" type="submit">Delete Backup</button>
                    </form>
                </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
<!--<pre><code>--><?php //print_r($eee); ?><!--</code></pre>-->
