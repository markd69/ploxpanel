<?php
$this->pageTitle = Yii::app()->name . ' - '.Yii::t('mc', 'Statistics');

$this->breadcrumbs=array(
    Yii::t('mc', 'Servers')=>array('index'),
    $model->name=>array('view', 'id'=>$model->id),
    Yii::t('mc', 'Statistics'),
);

$this->menu = array(
    array(
        'label'=>Yii::t('mc', 'Back'),
        'url'=>array('server/view', 'id'=>$model->id),
        'icon'=>'back',
    )
);

?>
<div class="row">
    <h3>
        <div class="pull-left" id="statusicon-ajax"><?php echo @$data['statusicon'] ?></div>
        <?php echo Yii::t('mc', 'Statistics') ?>
    </h3>
</div>
<br>
<div>
    <div class="row">
        <iframe src="<?php echo "https://d3w3usbi9msqwa.cloudfront.net/graphs/uptime?server={$model->ip}:{$model->port}&width=250px&gaugeHeight=125&gaugeWidth=175&valueFont=35&valLabelFont=15&labelFont=10"?>" frameborder="0" height="270" width="300">
        </iframe>
        <iframe src="<?php echo "https://d3w3usbi9msqwa.cloudfront.net/graphs/best?server={$model->ip}:{$model->port}"?>" frameborder="0" height="300" width="250">
        </iframe>
        <iframe src="<?php echo "https://d3w3usbi9msqwa.cloudfront.net/graphs/total?server={$model->ip}:{$model->port}&height=175px&valueFont=55"?>" frameborder="0" height="250" width="250">
        </iframe>
    </div>
    <div class="row">
        <iframe src="<?php echo "https://d3w3usbi9msqwa.cloudfront.net/graphs/top?server={$model->ip}:{$model->port}&width=350px"?>" frameborder="0" width="350" height="450">
        </iframe>
        <iframe src="<?php echo "https://d3w3usbi9msqwa.cloudfront.net/graphs/players?server={$model->ip}:{$model->port}&width=400px&graphHeight=325"?>" frameborder="0" width="450" height="500">
        </iframe>
    </div>
</div>
<div class="row text-center">
    <h5>Powered by <img src="https://i.imgur.com/ScC3w16.png" style="width:25px;"/> Negafinity &copy; 2020</h5>
</div>
