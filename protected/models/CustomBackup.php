<?php
/**
 *
 *   Copyright © 2010-2018 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/

class CustomBackup extends ActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'custom_backups';
    }
    public function rules()
    {
        return array(
            array('serverid', 'numerical'),
            array('daemonid', 'numerical'),
            array('status', 'numerical'),
            array('filename', 'string'),
            array('filesize', 'string'),
            array('owner_email', 'string'),
            array('date', 'string'),
            array('automated', 'number'),
        );
    }
}

