<?php
/**
 *
 *   Copyright © 2010-2018 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/

class Tool extends ActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'tools';
    }

    public function getDbConnection()
    {
        return Yii::app()->bridgeDb;
    }

    public function rules()
    {
        return array(
            array('placeholder', 'numerical')
        );
    }
    public function relations()
    {
        return array();
    }
}

