<?php
/**
 *
 *   Copyright © 2010-2019 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/

class RawHttpException extends CHttpException
{
}
