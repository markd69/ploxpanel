<?php /*** THIS FILE WAS GENERATED BY THE MULTICRAFT FRONT-END ***/
return array (
  'panel_db' => 'mysql:host=127.0.0.1;dbname=panel',
  'daemon_db' => 'mysql:host=127.0.0.1;dbname=daemon',
  'daemon_password' => 'none',
  'superuser' => 'admin',
  'api_enabled' => true,
  'api_allow_get' => false,
  'user_api_keys' => false,
  'admin_name' => 'Multicraft Administrator',
  'admin_email' => '',
  'show_serverlist' => 'user',
  'hide_userlist' => true,
  'ftp_client_disabled' => false,
  'ftp_client_passive' => false,
  'templates_disabled' => false,
  'ajax_updates_disabled' => false,
  'ajax_update_interval' => '2000',
  'timeout' => '5',
  'mark_daemon_offline' => '10',
  'theme' => 'ploxHostMC',
  'mobile_theme' => '',
  'user_theme' => false,
  'language' => '',
  'login_tries' => '4',
  'login_interval' => '300',
  'ajax_serverlist' => true,
  'status_banner' => true,
  'mail_welcome' => false,
  'mail_assign' => false,
  'sqlitecache_schema' => true,
  'sqlitecache_commands' => false,
  'user_mysql' => true,
  'user_mysql_host' => '',
  'user_mysql_user' => '',
  'user_mysql_pass' => '',
  'user_mysql_prefix' => '',
  'user_mysql_admin' => '',
  'show_repairtool' => 'none',
  'register_disabled' => false,
  'reset_token_hours' => '0',
  'default_ignore_ip' => false,
  'default_display_ip' => '',
  'show_memory' => true,
  'log_bottomup' => true,
  'admin_ips' => '',
  'api_ips' => '',
  'enable_csrf_validation' => true,
  'enable_cookie_validation' => true,
  'use_pluginlist' => true,
  'auto_jar_submit' => 'yes',
  'pw_crypt' => 'sha512_crypt',
  'ip_auth' => true,
  'cpu_display' => 'core',
  'ram_display' => '',
  'enable_disk_quota' => false,
  'block_chat_characters' => true,
  'log_console_commands' => false,
  'show_delete_all_players' => 'superuser',
  'kill_button' => 'user',
  'fill_port_gaps' => true,
  'support_legacy_daemons' => true,
  'user_subdomains' => false,
  'user_subdomains_domain' => 'plox.host',
  'user_subdomains_cf_user' => '',
  'user_subdomains_cf_api_key' => '',
  'panel_db_user' => 'root',
  'panel_db_pass' => 'cumberland',
  'daemon_db_user' => 'root',
  'daemon_db_pass' => 'cumberland',
  'support_url' => 'https://help.plox.host/',
  'staff_ips' => '',
  'enable_gauth' => true,
  'gauth_domain' => '',
  'gauth_single_login' => false,
  'editable_roles' => false,
  'min_pw_length' => '',
  'default_displayed_ip' => '',
  'show_daemon' => false,
  'user_subdomains_cf_email' => 'ploxhost@gmail.com',
  'user_subdomains_cf_key' => '61f49de33c350477b3f240a4c9b0a74bd247c',
  'user_subdomains_only_srv' => true,
  'user_subdomains_invalid' => 'fuck, ploxhost,plox.host, sucks',
  'ftp_client_ssl' => false,
  'ftp_client_web_zip' => false,
  'support_legacy_api' => true,
  'additional_ports' => '2',
  'additional_port_range' => '5000-9000',
);