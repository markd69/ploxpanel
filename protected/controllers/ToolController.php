<?php
/**
 *
 *   Copyright © 2010-2018 by xhost.ch GmbH
 *
 *   All rights reserved.
 *
 **/
include_once "protected/models/Server.php";

class ToolController extends Controller
{
    public $layout='//layouts/column2';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('motdcreator','votifiertest'),
                'users'=>array('*')
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex($sv)
    {
//        $model = $this->loadModel($sv);
//        $model->unsetAttributes();
        $this->render('index',array(
//            'model'=>$model,
        ));
    }

    public function actionVotifiertest($sv)
    {
//        $model = $this->loadModel($sv);
//        $model->unsetAttributes();
        $server = Server::model()->findByPk((int)$sv);
        $ip = $server === null ? "Server not found" : $server->ip;
        $port = $server === null ? "Server not found" : $server->port;
        $this->render('votifiertest',array(
//            'model'=>$model,
            'sv'=>$sv,
            'ip'=> $ip,
            'port'=> $port,
        ));
    }

    public function actionMotdcreator($sv)
    {
//        $model = $this->loadModel($sv);
//        $model->unsetAttributes();
        $this->render('motdcreator',array(
//            'model'=>$model,
            'sv'=>$sv
        ));
    }
    public function loadModel($id)
    {
        $model=Tool::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,Yii::t('mc', 'The requested page does not exist.'));
        return $model;
    }
}
